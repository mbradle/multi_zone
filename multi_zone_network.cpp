////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for running a multi-zone network calculation.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <fstream>
#include <iostream>

#ifdef WN_USER
#include "master.h"
#else
#include "default/master.h"
#endif

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] ) {

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the network and zone data.
  //============================================================================

  wn_user::network_data my_network_data( v_map );

  my_network_data();

  wn_user::zone_data my_zone_data( v_map, my_network_data.getNucnet() );

  wn_user::zone_linker my_zone_linker( v_map );

  //============================================================================
  // Create objects.
  //============================================================================

  wn_user::network_time my_network_time( v_map );

  wn_user::time_adjuster my_time_adjuster( v_map );

  wn_user::network_limiter my_limiter( v_map );

  wn_user::outputter my_outputter( v_map );

  wn_user::rate_modifier my_rate_modifier( v_map );

  wn_user::rate_registerer my_rate_registerer( v_map );

  wn_user::screener my_screener( v_map );

  wn_user::network_evolver my_evolver( v_map );

  wn_user::step_printer my_step_printer( v_map );

  wn_user::properties_updater my_properties_updater( v_map );

  //============================================================================
  // Get the zones to evolve.
  //============================================================================

  std::vector<nnt::Zone> zones = my_zone_data.createZones();

  //============================================================================
  // Initialize times.
  //============================================================================

  my_network_time.initializeTimes( zones );

  //============================================================================
  // Rate registration.
  //============================================================================

  my_rate_registerer( my_network_data.getNetwork() );

  //============================================================================
  // Screener.
  //============================================================================

  my_screener( zones );

  //============================================================================
  // Initial rate modifier.
  //============================================================================

  my_rate_modifier( zones );

  //============================================================================
  // Initial limiter.
  //============================================================================

  my_limiter( zones );

  //============================================================================
  // Set the output.
  //============================================================================

  my_outputter.set( my_network_data.getNucnet() );

  //============================================================================
  // Initialize properties.
  //============================================================================
  
  my_properties_updater.initialize( zones );
  
  //============================================================================
  // Evolve network while t < final t.
  //============================================================================

  while( my_network_time.isLessThanEndTime( zones ) )
  {

  //============================================================================
  // Update time.
  //============================================================================

  double d_dt = zones[0].getProperty<double>( nnt::s_DTIME );
  double d_time = zones[0].getProperty<double>( nnt::s_TIME );

  for( size_t i = 0; i < zones.size(); i++ )
  {
    zones[i].updateProperty( nnt::s_TIME, d_time + d_dt );
  }

  //============================================================================
  // Modify rates (and print those modified).
  //============================================================================

    my_rate_modifier( zones );

  //============================================================================
  // Evolve abundances.
  //============================================================================

    my_evolver( zones, my_zone_linker, d_dt );

  //============================================================================
  // Output.
  //============================================================================

    my_outputter( zones );

  //============================================================================
  // Print to screen.
  //============================================================================

    my_step_printer( zones );

  //============================================================================
  // Update properties.
  //============================================================================

    my_properties_updater( zones );

  //============================================================================
  // Update timestep.
  //============================================================================

    my_time_adjuster( zones );

  //============================================================================
  // Limit network.
  //============================================================================

    my_limiter( zones );

  }

  //============================================================================
  // Done.
  //============================================================================

  return EXIT_SUCCESS;

}
